FoamFile{
    format      ascii;
    class       dictionary;
    object      snappyHexMeshDict;
}

castellatedMesh true;
snap            true;
addLayers       true;

geometry
{
    cavity3Dpatches.stl
        {
        type triSurfaceMesh;
        name cavity3D;

        regions
        {
            FrontAndBack
                {
                name FrontAndBack;
            }

            MovingWall
                {
                name MovingWall;
            }

            FixedWalls
                {
                name FixedWalls;
            }
        }
    }
};

castellatedMeshControls{

    maxLocalCells 100000;
    maxGlobalCells 20000000;
    minRefinementCells 10;
    maxLoadUnbalance 0.10;
    nCellsBetweenLevels 2;
    features
        (
                {
       file "cavity3Dpatches.eMesh";
       level 0;
       }
        );
    refinementSurfaces
        {
        cavity3D
                {
            level (1 1);
        }
    }

        resolveFeatureAngle 30;
        planarAngle 30;
        refinementRegions
                {
        }

        locationInMesh (0.5 0.5 0.5);
        allowFreeStandingZoneFaces true;
        }

snapControls
        {
    nSmoothPatch 3;
    tolerance 2.0;
    nSolveIter 30;
    nRelaxIter 5;
    nFeatureSnapIter 10;
    implicitFeatureSnap false;
    explicitFeatureSnap true;
    multiRegionFeatureSnap false;
        }

addLayersControls
        {
    relativeSizes true;
    expansionRatio 1.2;
    finalLayerThickness 0.5;
    minThickness 0.1;
    layers
        {
        }
    nGrow 0;
    featureAngle 130;
    maxFaceThicknessRatio 0.5;
    nSmoothSurfaceNormals 1;
    nSmoothThickness 10;
    minMedialAxisAngle 90;
    minMedianAxisAngle 90;
    maxThicknessToMedialRatio 0.3;

    nSmoothNormals 3;
    slipFeatureAngle 30;
    nRelaxIter 5;
    nBufferCellsNoExtrude 0;
    nLayerIter 50;
    nRelaxedIter 20;
        }

meshQualityControls
        {
    #include "meshQualityDict"
    relaxed
        {
        maxNonOrtho 75;
        }
    nSmoothScale 4;
    errorReduction 0.75;
    }

debugFlags();
writeFlags();
mergeTolerance 1e-8;
